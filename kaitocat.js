#!/usr/bin/env node
var spawn   = require('child_process').spawn;
var join    = require('path').join;
var main    = join(__dirname, 'node_modules/');
var inherit = {stdio : 'inherit'};

if (process.env.NODE_ENV === 'dev')
{
  spawn(join(main, 'parallelshell/index.js'),
  [
    join(main, 'webpack-dev-server/bin/webpack-dev-server.js --hot --inline --progress --colors'),
    join(main, 'nodemon/bin/nodemon.js')
  ], inherit);
}
else
{
  spawn(join(main, 'webpack/bin/webpack.js'), ['-p'], inherit);
}
