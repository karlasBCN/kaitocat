var knox   = require('knox');
var config = require('./config');

var s3Client = knox.createClient(
{
  key    : config.amazon_key,
  secret : config.amazon_secret,
  bucket : 'sobrassada.stl'
});

module.exports = s3Client;
