var db          = null;
var mongodb     = require('mongodb');
var MongoClient = mongodb.MongoClient;
var ObjectID    = mongodb.ObjectID;
var config      = require('./config');

module.exports =
{
  init: function(callback)
  {
    MongoClient.connect(config.mongodb_uri, function(err, _db)
    {
      if(err)
      {
        throw err;
      }
      db = _db;
      callback();
    });
  },
  getCollection : function(c)
  {
    return db.collection(c);
  },
  _id : function(id)
  {
    return {_id : new ObjectID(id)};
  }
};
