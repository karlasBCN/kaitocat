var Collection       = require('ampersand-collection');
var Figure           = require('./figure_model');
var figureCollection = Collection.extend({model : Figure});

module.exports = figureCollection;
