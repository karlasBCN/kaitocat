var db          = require('../../db');
var Figure      = require('./figure_model');
var collection  = db.getCollection('sbrssd_figures');

var getInstance = function(data)
{
  data.id = data._id + '';
  return new Figure(data);
};

var provider =
{
  getByUserId : function(userId, callback)
  {
    collection.find({userId : userId}).toArray(function(err, data)
    {
      if (err)
      {
        throw err;
      }
      callback(data.map(getInstance));
    });
  },
  insertFigure : function(userId, name, callback)
  {
    var attrs =
    {
      name     : name,
      userId   : userId
    };
    collection.insert(attrs, function(err, result)
    {
      if (err)
      {
        throw err;
      }
      callback(getInstance(result.ops[0]));
    });
  },
  removeFigure: function(id, callback)
  {
    collection.remove(db._id(id), function(err, result){
      callback(err ? false : (result.result.n === 1));
    });
  }
};

module.exports = provider;
