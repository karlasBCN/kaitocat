var State  = require('ampersand-state');
var Figure = State.extend(
{
  props: {
    id     : 'string',
    userId : 'string',
    name   : 'string'
  }
});

module.exports = Figure;
