var state            = require('ampersand-state');
var bcrypt           = require('bcrypt');
var fp               = require('../Figure/figure_provider');
var isFunction       = require('lodash.isfunction');

var User = state.extend({
  props:
  {
    email    : 'string',
    password : 'string',
    name     : 'string',
    id       : 'string'
  },
  authenticate : function(clear, callback)
  {
    bcrypt.compare(clear, this.password, function(err, ok)
    {
      if (err)
      {
        throw err;
      }
      callback(ok);
    });
  },
  clientJson : function()
  {
    var json = this.toJSON();
    delete json.password;
    return json;
  },
  fetchFigures : function(callback)
  {
    var _this = this;
    fp.getByUserId(_this.id, function(figures)
    {
      isFunction(callback) && callback(figures);
    });
  }
});

module.exports = User;
