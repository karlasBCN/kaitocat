var db          = require('../../db');
var User        = require('./user_model');
var bcrypt      = require('bcrypt');
var collection  = db.getCollection('sbrssd_users');
var getInstance = function(data)
{
  Object.assign(data,
  {
    id      : data._id + '',
    figures : []
  });
  return new User(data);
};

var provider =
{
  getById : function(id, callback)
  {
    collection.findOne(db._id(id), function(err, data)
    {
      if (err)
      {
        throw err;
      }
      callback((data === null) ? null : getInstance(data));
    });
  },
  getByEmail : function(email, callback)
  {
    collection.findOne({email : email}, function(err, data)
    {
      if (err)
      {
        throw err;
      }
      callback((data === null) ? null : getInstance(data));
    });
  },
  insertUser : function(email, password, name, callback)
  {
    this.getByEmail(email, function(user)
    {
      if (user !== null)
      {
        callback(false);
      }
      else
      {
        bcrypt.genSalt(10, function(err, salt)
        {
          if (err)
          {
            throw err;
          }
          bcrypt.hash(password, salt, function(_err, hash)
          {
            if (_err)
            {
              throw _err;
            }
            var attrs =
            {
              email    : email,
              password : hash,
              name     : name
            };
            collection.insert(attrs, function(__err, result)
            {
              if (__err)
              {
                throw __err;
              }
              callback(getInstance(result.ops[0]));
            });
          });
        });
      }
    });
  }
};

module.exports = provider;
