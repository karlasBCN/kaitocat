var fs  = require('fs');
var env = process.env;

//Heroku

var production = function()
{
  return {
    'mongodb_uri'   : env.MONGODB_URI,
    'amazon_key'    : env.AMAZON_KEY,
    'amazon_secret' : env.AMAZON_SECRET,
    'cookie_secret' : env.COOKIE_SECRET,
    'max_upload'    : parseInt(env.MAX_UPLOAD, 10)
  };
};

//Dev

var dev = function()
{
    var file = './dev.config.json';
    if (!fs.existsSync(file))
    {
      throw ('You need dev.config.json file at root of project in dev mode!!!!!!');
    }
    return JSON.parse(fs.readFileSync(file));
};

module.exports = (env.NODE_ENV === 'production') ? production() : dev();
