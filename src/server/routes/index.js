module.exports = {
  'sobrassada.kaito.cat' : require('./sobrassada'),
  'www.kaito.cat'        : require('./www'),
  'proxy.kaito.cat'      : require('./proxy')
};
