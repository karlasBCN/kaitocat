var provider = require('../../../Models/User/user_provider');

module.exports =
[
  {
    method  : 'GET',
    path    :'/',
    handler : function (request, reply) {
      provider.getByEmail('carles.pages.rozas@gmail.com', function(user)
      {
        reply.view('www/index.html', {name : user.name});
      });
    }
  }
];
