var isDev          = (process.env.NODE_ENV === 'dev');
var lastConnection = 0;

var message = function()
{
  return {
    lastConnection : lastConnection
  };
};

module.exports =
[
  {
    method: 'GET',
    path:'/',
    handler: function (request, reply) {
      var assetsHost = isDev ? '//localhost:8080' : '';
      reply.view('proxy/index.html', {
        first      : JSON.stringify(message()),
        assetsPath : `${assetsHost}/public`
      });
    }
  },
  {
    method: 'POST',
    path:'/',
    handler: function (request, reply){
      lastConnection = Math.floor(Date.now() / 1000);
      reply({ok : 69});
    }
  },
  {
    method: 'POST',
    path:'/getter',
    handler: function (request, reply){
      reply(message());
    }
  }
];
