var path           = require('path');
var figure_routes  = require('./figures');
var provider       = require('../../../Models/User/user_provider');
var config         = require('../../../config');
var isDev          = (process.env.NODE_ENV === 'dev');

var defaultHandler = function(request, reply)
{
  if (request.params.route && request.params.route.includes('favicon.ico'))
  {
    reply.file(path.join(__dirname, '../../../../favicon.ico'));
    return;
  }

  var userId   = request.yar.get('userId');
  var conf     = {maxUpload : config.max_upload};
  var response = function()
  {
    reply.view('sobrassada/index.html',
    {
      config     : JSON.stringify(conf),
      assetsPath : `${isDev ? '//localhost:8080' : ''}/public`
    });
  };

  if (userId)
  {
    provider.getById(userId, function(_user)
    {
      _user.fetchFigures(function(figures)
      {
        Object.assign(conf,
        {
          user : _user.clientJson(),
          figures
        });
        response();
      });
    });
  }
  else
  {
    response();
  }
};

module.exports = figure_routes.concat(
[
  {
    method  : 'GET',
    path    : '/{route?}',
    handler : defaultHandler
  },
  {
    method  : 'GET',
    path    : '/viewFigure/{route?}',
    handler : defaultHandler
  },
  {
    method  : 'GET',
    path    : '/logout',
    handler : function(request, reply)
    {
      request.yar.reset();
      reply.redirect('/');
    }
  },
  {
    method  : 'POST',
    path    : '/login',
    handler : function(request, reply)
    {
      var post = request.payload;
      provider.getByEmail(post.email, function(user)
      {
        if (user)
        {
          user.authenticate(post.password, function(_ok)
          {
            if (_ok)
            {
              request.yar.set('userId', user.id);
              user.fetchFigures(function(figures)
              {
                reply({
                  user : user.clientJson(),
                  figures
                });
              });
            }
            else
            {
              reply({error : true});
            }
          });
        }
        else
        {
          reply({error : true});
        }
      });
    }
  },
  {
    method  : 'POST',
    path    : '/register',
    handler : function(request, reply)
    {
      var post = request.payload;
      provider.insertUser(post.email, post.password, post.name, function(user)
      {
        if (!user)
        {
          reply({error : true});
        }
        else
        {
          request.yar.set('userId', user.id);
          reply(user.clientJson());
        }
      });
    }
  }
]);
