var s3Client   = require('../../../s3Client');
var config     = require('../../../config');
var fProvider  = require('../../../Models/Figure/figure_provider');
var uProvider  = require('../../../Models/User/user_provider');
var async      = require('async');
var capitalize = require('lodash.capitalize');

module.exports =
[
  {
    method : 'POST',
    path   : '/upload_figure',
    config :
    {
      payload :
      {
          output   : 'stream',
          parse    : true,
          allow    : 'multipart/form-data',
          maxBytes : config.max_upload * 1024 * 1024
      },
      handler : function(request, reply)
      {
        var file    = request.payload.file;
        var userId  = request.yar.get('userId');
        var name    = capitalize(file.hapi.filename.split('.')[0]);
        var headers = {'content-type': 'application/octet-stream'};

        if (!userId)
        {
          reply({error : 'No active session'});
          return;
        }

        fProvider.insertFigure(userId, name, function(figure)
        {
          s3Client.putBuffer(file._data, '/' + figure.id, headers, function(err)
          {
            reply(err ? {error : err} : figure.toJSON());
          });
        });
      }
    }
  },
  {
    method  : 'GET',
    path    : '/download_figure/{id}',
    handler : function(request, reply)
    {
      var figureId = request.params.id;
      var userId   = request.yar.get('userId');

      if (!userId)
      {
        reply({error : 'No active session'});
        return;
      }

      uProvider.getById(userId, function(user)
      {
        user.fetchFigures(function(figures)
        {
          if (figures.find(figure => figureId === figure.id))
          {
            s3Client.getFile('/' + figureId, function(err, res)
            {
              reply(err ? {error : 'Error getting file from s3'} : res);
            });
          }
          else
          {
            reply({error : 'Figure does not belong to user'});
          }
        });
      });
    }
  },
  {
    method  : 'POST',
    path    : '/remove_figure',
    handler : function(request, reply)
    {
      var figureId = request.payload.id;
      var userId   = request.yar.get('userId');

      uProvider.getById(userId, function(user)
      {
        user.fetchFigures(function(figures)
        {
          if (figures.find(figure => figureId === figure.id))
          {
            async.parallel(
              [
                function(end){
                  fProvider.removeFigure(figureId, function(ok){
                    end(null, ok);
                  });
                },
                function(end){
                  s3Client.deleteFile('/' + figureId, function(err){
                    end(null, (err ? false : true));
                  });
                }
              ],
              function(err, results){
                if (err)
                {
                  throw 'Error removing figure';
                }
                reply(results[0] && results[1]);
              }
            );
          }
          else
          {
            reply({error : 'Figure does not belong to user'});
          }
        });
      });
    }
  }
];
