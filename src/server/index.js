var Hapi    = require('hapi');
var forEach = require('lodash.foreach');
var path    = require('path');
var yar     = require('yar');
var inert   = require('inert');
var vision  = require('vision');
var ejs     = require('ejs');
var routes  = require('./routes');
var config  = require('../config');
var isDev   = process.env.NODE_ENV === 'dev';
var server  = new Hapi.Server();

// Set connection params
server.connection(
{
  host : isDev ? '127.0.0.1' : '0.0.0.0',
  port : process.env.PORT || 3000
});

//Routes
forEach(routes, function(_routes, host)
{
  forEach(_routes, function(opts)
  {
    Object.assign(opts, {vhost : host});
    server.route(opts);
  });
});

//Session
server.register(
{
  register: yar,
  options:
  {
    cookieOptions:
    {
      password : config.cookie_secret,
      isSecure : !isDev
    }
  }
}, function (err)
{
  if (err)
  {
    throw err;
  }
});

//Views
server.register(vision, function()
{
  server.views(
  {
    engines : {html: ejs},
    path    : path.join(__dirname, '/../../templates')
  });
});

//Assets
server.register(inert, function(){
  if (!isDev)
  {
    server.route(
    {
      method  : 'GET',
      path    : '/public/{file*}',
      handler : {directory: {path: './public'}}
    });
  }
  server.route(
  {
    method  : 'GET',
    path    : '/favicon.ico',
    handler : {file: {path: './favicon.ico'}}
  });
});


module.exports =
{
  init : function(callback)
  {
    // Start the server
    server.start(callback);
  }
};
