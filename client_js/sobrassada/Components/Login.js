import React         from 'react';
import isEmail       from 'is-email';
import FormGroup     from 'react-bootstrap/lib/FormGroup';
import FormControl   from 'react-bootstrap/lib/FormControl';
import ControlLabel  from 'react-bootstrap/lib/ControlLabel';
import Button        from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import ajax          from '../ajax';
import router        from '../router';
import ErrorMessage  from './ErrorMessage';

class Login extends React.Component
{
  constructor()
  {
    super();
    this.state = {email : '', password : ''};
  }
  handleChange(e)
  {
    const {value, name} = e.target;
    this.setState({[name] : value});
  }
  handleSubmit(e)
  {
    const {state, props, refs}  = this;
    const {error}               = refs;
    const {setUser, setFigures} = props;
    const {email, password}     = state;

    e.preventDefault();

    if (!isEmail(email))
    {
      error.show('Not valid email');
      return;
    }

    if (password.length < 8)
    {
      error.show('Password must have at least 8 characters');
      return;
    }

    ajax('/login', state, function(resp)
    {
      const {figures, user} = resp;
      if (resp.error)
      {
        error.show('Incorrect user or password');
      }
      else
      {
        setFigures(figures);
        setUser(user);
      }
    });

  }
  render()
  {
    const change = this.handleChange.bind(this);
    const {username, password} = this.state;

    return <div>
      <ErrorMessage ref="error" />
      <form onSubmit={this.handleSubmit.bind(this)}>
        <FormGroup>
          <ControlLabel>Email Address</ControlLabel>
          <FormControl onChange={change} value={username} type='email' name='email' placeholder='Enter email' />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Password</ControlLabel>
          <FormControl onChange={change} value={password} type='password' name='password' />
        </FormGroup>
        <ButtonToolbar>
          <Button bsStyle='primary' type='submit'>Login</Button>
          <Button bsStyle='info' onClick={() => router.navigate('register')}>Register</Button>
        </ButtonToolbar>
      </form>
    </div>;
  }
};

export default Login;
