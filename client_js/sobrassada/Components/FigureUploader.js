import React         from 'react';
import isBinary      from 'is-binary';
import Upload        from 'upload-component-browserify';
import Panel         from 'react-bootstrap/lib/Panel';
import Button        from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import ProgressBar   from 'react-bootstrap/lib/ProgressBar';
import ErrorMessage  from './ErrorMessage';

var initialState = {
  uploading : false,
  progress  : 0
};

class FigureUploader extends React.Component
{
  constructor()
  {
    super();
    this.state = initialState;
  }
  trigger()
  {
    this.refs.input.click();
  }
  handleFile(e)
  {
    var _this     = this;
    var reader    = new FileReader();
    var file      = e.target.files[0];
    const {maxUpload, addNewFigure} = this.props;
    var error     = function(msg)
    {
      _this.refs.error.show(msg);
      _this.setState(initialState);
    };

    _this.setState({uploading : true});

    reader.onload = function(f) {
      var content = f.target.result;
      if (isBinary(content))
      {
        error('Binary files are not allowed at this moment');
      }
      else if (content.length > (maxUpload * 1024 * 1024))
      {
        error(`Maximum size is ${maxUpload} Mb`);
      }
      else
      {
        var up = new Upload(file);
        up.on('progress', function(progress)
        {
          _this.setState({progress : progress.percent});
        });
        up.on('end', function(result)
        {
          var response = JSON.parse(result.response);
          if (response.error)
          {
            error('Sorry! There was an error uploading');
          }
          else
          {
            response.content = content;
            addNewFigure(response);
            _this.setState(initialState);
          }
        });
        up.to('/upload_figure');
      }
    };
    reader.readAsText(file);
  }
  render()
  {
    const {state, props, trigger, handleFile} = this;
    const {progress, uploading} = state;
    let view  = null;

    if (uploading)
    {
      view = <div>
        <p>Uploading file, please wait...</p>
        <ProgressBar now={progress} />
      </div>;
    }
    else
    {
      view = <div>
        <Button bsStyle='primary' onClick={trigger.bind(this)}>
          Select STL ASCII File (max size: {props.maxUpload}Mb)
        </Button>
        <input
          ref="input"
          onChange={handleFile.bind(this)}
          accept=".stl"
          type='file'
          style={{display : 'none'}}
        />
      </div>;
    }
    return <Panel header='Upload a new figure'>
      <ErrorMessage ref="error" />
      {view}
      <h6>Grab some example STL ASCII files (use "Save as" in context menu):</h6>
      <ButtonToolbar>
        <Button bsSize="xs" href="http://people.sc.fsu.edu/~jburkardt/data/stla/magnolia.stl">Magnolia</Button>
        <Button bsSize="xs" href="http://people.sc.fsu.edu/~jburkardt/data/stla/space_invader_magnet.stl">Space Invader</Button>
      </ButtonToolbar>
    </Panel>;
  }
};

export default FigureUploader;
