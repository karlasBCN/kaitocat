import React from 'react';
import Alert from 'react-bootstrap/lib/Alert';
import Fade  from 'react-bootstrap/lib/Fade';

var displayTime  = 5000;
var fadeTime     = 200;
var initialState = {message : ''};

class ErrorMessage extends React.Component
{
  constructor()
  {
    super();
    this.state = initialState;
  }
  show(msg)
  {
    var _this = this;
    if (_this.state.message !== '')
    {
      return;
    }
    _this.setState({message : msg});
    setTimeout(function(){
      _this.setState(initialState);
    }, displayTime);
  }
  render()
  {
    var message = this.state.message;

    return <Fade in={message !== ''} timeout={fadeTime} unmountOnExit={true}>
      <Alert bsStyle="danger">
        <strong>{message}</strong>
      </Alert>
    </Fade>;
  }
};

export default ErrorMessage;
