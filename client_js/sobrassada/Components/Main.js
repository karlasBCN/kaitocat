import React      from 'react';
import PageHeader from 'react-bootstrap/lib/PageHeader';
import Login      from './Login';
import Register   from './Register';
import Home       from './Home';
import app        from '../app';

class Main extends React.Component
{
  render()
  {
    let view = null;
    const { user, figures, route, routeParam } = this.props;

    switch(route)
    {
      case 'main'       :
        view = (user ? <Home figures={figures} viewFigure={false} /> : <Login {...this.props} />);
      break;
      case 'viewFigure' :
        view = <Home figures={figures} viewFigure={routeParam} />;
      break;
      case 'register'   :
        view = <Register />;
      break;
    }

    return <div className="container">{view}</div>;
  }
};

export default Main;
