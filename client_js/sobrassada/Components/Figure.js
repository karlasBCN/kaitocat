import React         from 'react';
import ReactDOM      from 'react-dom';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import dt            from 'ua-device-type';
import FigureModal   from './FigureModal';
import ConfirmModal  from './ConfirmModal';
import router        from '../router';
import ajax          from '../ajax';

class Figure extends React.Component
{
  constructor()
  {
    super();
    this.state = {
      showRemove       : false,
      showConfirmModal : false,
      isDesktop        : dt(navigator.userAgent) === 'desktop'
    };
  }
  toggleRemove(enter)
  {
    var _this = this;
    var show  = _this.state.isDesktop && enter;
    return function()
    {
      _this.setState({showRemove : show});
    }.bind(_this);
  }
  openFigureModal()
  {
    router.navigate(`viewFigure/${this.props.figure.id}`);
  }
  toggleConfirmModal(e)
  {
    var _this = this;
    if (e)
    {
      e.stopPropagation();
    }
    _this.setState(
    {
      showConfirmModal : !_this.state.showConfirmModal,
      showRemove       : false
    });
    ReactDOM.findDOMNode(_this).blur();
  }
  render()
  {
    var _this            = this;
    var state            = _this.state;
    var props            = _this.props;
    var figure           = props.figure;
    var toggleConfirm    = _this.toggleConfirmModal.bind(_this);
    var toggleRemove     = _this.toggleRemove.bind(_this);
    var viewFigure       = props.viewFigure;
    var showConfirmModal = state.showConfirmModal;
    var showRemove       = state.showRemove && (!viewFigure) && (!showConfirmModal);
    var componentProps   =
    {
      onClick      : _this.openFigureModal.bind(_this),
      onMouseEnter : toggleRemove(true),
      onMouseLeave : toggleRemove(false)
    };
    var modalProps = viewFigure ?
    {
      figure,
      isDesktop     : state.isDesktop,
      setSTLContent : props.setSTLContent,
    } : null;
    var confirmProps = showConfirmModal ?
    {
      textContent   : `Do you really want to delete figure "${figure.name}"?`,
      title         : 'Delete figure',
      onHide        : toggleConfirm,
      confirmAction : function()
      {
        ajax('/remove_figure', {id : figure.id}, function(){});
        props.removeFigure()
      }
    } : null;

    return <ListGroupItem {...componentProps} >
      {figure.name}
      {showRemove ? <i onClick={toggleConfirm} className='fa fa-times pull-right' /> : null}
      {viewFigure ? <FigureModal {...modalProps} /> : null}
      {showConfirmModal ? <ConfirmModal {...confirmProps} /> : null}
    </ListGroupItem>;
  }
};

export default Figure;
