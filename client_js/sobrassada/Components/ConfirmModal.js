import React  from 'react';
import Modal  from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';

class ConfirmModal extends React.Component
{
  render(){

    var props  = this.props;
    var cancel = props.onHide;
    var ok     = function()
    {
      props.confirmAction();
      cancel();
    };

    return <Modal show={true} onHide={cancel} bsStyle='primary' animation={false}>
      <Modal.Header>
        <Modal.Title>{props.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>{props.textContent}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle='danger' onClick={ok}>{props.textOk}</Button>
        <Button onClick={cancel}>{props.textCancel}</Button>
      </Modal.Footer>
    </Modal>;
  }
};

ConfirmModal.defaultProps = {
  textOk      : 'Yes',
  textCancel  : 'Cancel',
  textContent : 'Are you sure you want to do that?',
  title       : 'Confirmation'
};

export default ConfirmModal;
