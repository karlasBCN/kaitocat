import React         from 'react';
import {findDOMNode} from 'react-dom';
import defer         from 'lodash.defer';
import parallel      from 'run-parallel';
import ajax          from '../ajax';

class FigureViewer extends React.Component
{
  constructor()
  {
    super();
    this.state = {loading : true};
  }
  shouldComponentUpdate(nextProps, nextState)
  {
    return (nextState.loading !== this.state.loading);
  }
  componentWillUnmount()
  {
    if (this.viewer)
    {
      this.viewer.stopRender();
      delete this.viewer;
    }
    else
    {
      this.avoidRender = true;
    }
  }
  fetch()
  {
    const _this = this;
    const {setSTLContent, id, content} = _this.props;

    parallel(
    [
      function(ok)
      {
        require.ensure([], function()
        {
          _this.viewer = require('../3DViewer').default;
          ok();
        }, '3DViewer');
      },
      function(ok)
      {
        if (content)
        {
          ok();
        }
        else
        {
          ajax('/download_figure/' + id, null, function(_content)
          {
            setSTLContent(_content);
            ok();
          }, true);
        }
      }
    ],
    function()
    {
      _this.setState({loading: false});
    })
  }
  canvasRender()
  {
    if (this.avoidRender)
    {
      delete this.avoidRender;
      return;
    }
    defer(() => this.viewer.canvasRender(findDOMNode(this), this.props.content));
  }
  render()
  {
    const {content, dim} = this.props;
    const {loading} = this.state;

    if (loading)
    {
      this.fetch();
    }
    else
    {
      this.canvasRender();
    }

    return <div className="canvasContainer" style={{height: dim, width : dim}}>
      {loading && (!content) ? <div><i className="fa fa-refresh fa-spin" /> Loading figure. Please wait...</div> : null}
    </div>;
  }
};

export default FigureViewer;
