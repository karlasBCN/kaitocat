import React        from 'react';
import Modal        from 'react-bootstrap/lib/Modal';
import Button       from 'react-bootstrap/lib/Button';
import FigureViewer from './FigureViewer';
import router       from '../router';

class FigureModal extends React.Component
{
  render()
  {
    const close   = () => router.navigate('');
    const {props} = this;
    const {id, name, content} = props.figure;

    const viewerProps =
    {
      id,
      content,
      dim           : props.isDesktop ? 420 : 280,
      setSTLContent : props.setSTLContent
    };

    return <Modal show={true} onHide={close} bsStyle='primary' animation={false}>
      <Modal.Header>
        <Modal.Title>{name}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FigureViewer {...viewerProps} />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={close}>Close</Button>
      </Modal.Footer>
    </Modal>;
  }
};

export default FigureModal;
