import React          from 'react';
import Grid           from 'react-bootstrap/lib/Grid';
import Row            from 'react-bootstrap/lib/Row';
import ListGroup      from 'react-bootstrap/lib/ListGroup';
import Figure         from './Figure';
import FigureUploader from './FigureUploader';

class Home extends React.Component
{
  render()
  {
    const {user, figures, setSTLContent, removeFigure,
           addNewFigure, viewFigure, maxUpload} = this.props;
    const figureList = figures.map(function(figure)
    {
      const {id}  = figure;
      const props =
      {
        figure,
        viewFigure    : viewFigure === id,
        setSTLContent : stl => setSTLContent(id, stl),
        removeFigure  : ()  => removeFigure(id)
      };
      return <Figure key={id} {...props} />;
    });

    return <Grid>
      <Row className="welcome">
        <h4>Welcome To Sobrassada, {user.name}!</h4>
        <a href="/logout">Logout</a>
      </Row>
      <Row>
        <ListGroup>{figureList}</ListGroup>
      </Row>
      <Row>
        <FigureUploader addNewFigure={addNewFigure} maxUpload={maxUpload}/>
      </Row>
    </Grid>;
  }
};

export default Home;
