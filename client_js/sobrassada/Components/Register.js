import React         from 'react';
import isEmail       from 'is-email';
import FormGroup     from 'react-bootstrap/lib/FormGroup';
import FormControl   from 'react-bootstrap/lib/FormControl';
import ControlLabel  from 'react-bootstrap/lib/ControlLabel';
import Button        from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import ErrorMessage  from './ErrorMessage';
import ajax          from '../ajax';
import router        from '../router';

const back = () => router.navigate('');

class Register extends React.Component
{
  constructor()
  {
    super();
    this.state = {email : '', password : '', password2 : '', name : ''};
  }
  handleChange(e)
  {
    const {value, name} = e.target;
    this.setState({[name] : value});
  }
  handleSubmit(e)
  {
    const {state, props, refs}               = this;
    const {error}                            = refs;
    const {email, password, password2, name} = state;

    e.preventDefault();

    if (!isEmail(email))
    {
      error.show('Not valid email!');
      return;
    }

    if (name.length < 3)
    {
      error.show('Name must have at least 3 characters!');
      return;
    }

    if (password !== password2)
    {
      error.show('Passwords do not match!');
      return;
    }

    if (password.length < 8)
    {
      error.show('Password must have at least 8 characters!');
      return;
    }

    ajax('/register', {email, password, name}, function(resp)
    {
      if (resp.error)
      {
        error.show('Sorry, there is another user with that email!');
      }
      else
      {
        props.setUser(resp);
        back();
      }
    });
  }
  render()
  {
    const change = this.handleChange.bind(this);
    const {email, password, password2, name} = this.state;

    return <div>
      <ErrorMessage ref="error" />
      <form onSubmit={this.handleSubmit.bind(this)}>
        <FormGroup>
          <ControlLabel>Email Address</ControlLabel>
          <FormControl onChange={change} value={email} type='email' name='email' placeholder='Enter email' />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Name</ControlLabel>
          <FormControl onChange={change} value={name} type='text' name='name' placeholder='Enter your name' />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Password</ControlLabel>
          <FormControl onChange={change} value={password} type='password' name='password' />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Repeat Password</ControlLabel>
          <FormControl onChange={change} value={password2} type='password' name='password2' />
        </FormGroup>
        <ButtonToolbar>
          <Button bsStyle='primary' type='submit'>Register</Button>
          <Button bsStyle='info' onClick={back}>Back</Button>
        </ButtonToolbar>
      </form>
    </div>;
  }
};

export default Register;
