import React      from 'react';
import PageHeader from 'react-bootstrap/lib/PageHeader';
import {connect}  from 'react-redux';
import {setUser}  from './State/User';
import {setFigures, setSTLContent, removeFigure, addNewFigure}
                  from './State/Figure';
import Login      from './Components/Login';
import Register   from './Components/Register';
import Home       from './Components/Home';

class App extends React.Component
{
  getLogged()
  {
    const {route, dispatch, user, figures, maxUpload} = this.props;

    if (!['viewFigure', 'main'].includes(route.path))
    {
      return <div />;
    }

    const props = {user, figures, maxUpload,
      viewFigure    : route.param || false,
      setSTLContent : (idFigure, stl) => dispatch(setSTLContent(idFigure, stl)),
      removeFigure  : idFigure => dispatch(removeFigure(idFigure)),
      addNewFigure  : data => dispatch(addNewFigure(data))
    };

    return <Home {...props} />;
  }
  getNotLogged()
  {
    const { route, dispatch } = this.props;
    const props =
    {
      setUser    : data => dispatch(setUser(data)),
      setFigures : data => dispatch(setFigures(data))
    };

    switch (route.path)
    {
      case 'main':
        return <Login {...props} />;
        break
      case 'register':
        return <Register {...props} />;
        break;
      default:
        return <div />;
    }
  }
  render()
  {
    return <div className="container">
      <PageHeader>Sobrassada Project</PageHeader>
      {this.props.user ? this.getLogged() : this.getNotLogged()}
    </div>;
  }
};

const select = function(state) {
  const  {user, figures, route, maxUpload} = state;
  return {user, figures, route, maxUpload};
};

export default connect(select)(App);
