import xhr from 'xhr';

var Ajax = function(url, obj, callback, useGet)
{
  var isGet  = (useGet === true);
  var params =
  {
    uri    : url,
    method : isGet ? 'GET' : 'POST'
  };

  (!isGet) && Object.assign(params,
  {
    body    : JSON.stringify(obj),
    headers : {'Content-Type': 'application/json'}
  });

  xhr(params, function (err, resp, body)
  {
    if (err)
    {
      throw 'Error with ajax request';
    }
    callback(isGet ? body : JSON.parse(body));
  });
};

export default Ajax;
