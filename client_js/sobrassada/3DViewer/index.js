import THREE from './three_stl';

var renderer         = new THREE.WebGLRenderer({antialias: true});
var camera           = new THREE.PerspectiveCamera(35, 1, 1, 10000);
var material         = new THREE.MeshPhongMaterial({color: 0x2c3e50});
var light            = new THREE.AmbientLight(0x337ab7);
var directionalLight = new THREE.DirectionalLight(0xffffff, 1);
var scene, requestAnimation;

//Creates a canvas in a targetDOM with a Three scene with given content
var canvasRender = function(targetDom, content)
{
  var mesh, dims = targetDom.getBoundingClientRect();

  var init = function()
  {
    scene  = new THREE.Scene();

    // object
    var loader = new THREE.STLLoader();

    loader.addEventListener('load', function (event){
      var geometry = event.content;
      var sizes    = geometry.boundingBox.size();

      mesh = new THREE.Mesh(geometry, material);
      scene.add(mesh);
      camera.position.set(3, 0.5, 3);
      camera.position.x = 2.26 * Math.max(sizes.x, sizes.y, sizes.z);
      camera.lookAt(scene.position);
    });

    // STL content to be loaded
    loader.loadFromContent(content);

    // lights
    scene.add(light);
    directionalLight.position.copy(camera.position);
    scene.add(directionalLight);

    // renderer
    renderer.setSize(dims.width, dims.height);
    targetDom.appendChild(renderer.domElement);
  };

  var render = function()
  {
    mesh.rotation.y = Date.now() * 0.0005;
    renderer.render(scene, camera);
    renderer.setClearColor(0xfbfbfb, 1);
  };

  var animate = function()
  {
    requestAnimation = requestAnimationFrame(animate);
    render();
  };

  init();
  animate();
};

var viewer =
{
  canvasRender : canvasRender,
  stopRender   : () => cancelAnimationFrame(requestAnimation)
};

export default viewer;
