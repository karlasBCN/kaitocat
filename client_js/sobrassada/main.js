import React      from 'react';
import {render}   from 'react-dom';
import {Provider} from 'react-redux';
import router     from './router';
import store      from './State/';
import App        from './app';

import '../../less/sobrassada/main.less';

router.start(store);

render(<Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('main')
);
