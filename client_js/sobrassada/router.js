import Router     from 'ampersand-router';
import {setRoute} from './State/Route';

let store = null;

const set = function(path, param = null)
{
  store.dispatch(setRoute(path, param));
};

var _router = Router.extend(
{
 routes:
 {
   '(/:any)'             : 'main',
   'register'            : 'register',
   'viewFigure/:figureId' : 'viewFigure'
 },
 main: function()
 {
   set('main');
 },
 register: function()
 {
   if (store.getState().user)
   {
     this.navigate('');
   }
   else
   {
     set('register');
   }
 },
 viewFigure: function(figureId)
 {
   const {user, figures} = store.getState();
   if (user && figures.find(figure => figure.id === figureId))
   {
     set('viewFigure', figureId);
   }
   else
   {
     this.navigate('');
   }
 }
});

var instance = new _router();

instance.start = function(_store)
{
  store = _store;
  this.history.start();
};

export default instance;
