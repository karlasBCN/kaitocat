const DO_LOGIN = 'User/doLogin';

const UserReducer = function(user = false, action)
{
  switch (action.type) {
    case DO_LOGIN:
      return action.data;
      break;
    default:
      return user;
  }
};

export function setUser(data)
{
  return {type: DO_LOGIN, data};
}

export default UserReducer;
