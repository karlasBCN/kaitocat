const SET_FIGURES     = 'Figure/setFigures';
const SET_STL_CONTENT = 'Figure/setSTLContent';
const REMOVE_FIGURE   = 'Figure/removeFigure';
const ADD_NEW_FIGURE  = 'Figure/addNewFigure';

const FigureReducer = function(figures = [], action)
{
  switch (action.type)
  {
    //List of user figures
    case SET_FIGURES:
      return action.data.map(figure => Object.assign(
        {content : ''},
        figure
      ));
      break;
    //Set figure STL
    case SET_STL_CONTENT:
      return figures.map(function(figure)
      {
        if (figure.id === action.idFigure)
        {
          figure.content = action.content;
        }
        return figure;
      });
      break;
    //Remove figure from list
    case REMOVE_FIGURE:
      return figures.filter(figure => figure.id !== action.idFigure);
      break;
    //Add new figure
    case ADD_NEW_FIGURE:
      return [...figures, action.data];
      break;
    default:
      return figures;
  }
};

export function setFigures(data)
{
  return {type: SET_FIGURES, data};
}

export function setSTLContent(idFigure, content)
{
  return {type: SET_STL_CONTENT, idFigure, content};
}

export function removeFigure(idFigure)
{
  return {type: REMOVE_FIGURE, idFigure};
}

export function addNewFigure(data)
{
  return {type: ADD_NEW_FIGURE, data};
}

export default FigureReducer;
