const SET_ROUTE    = 'Route/setRoute';
const defaultRoute = {path : 'main', param : null};

const RouteReducer = function(route = defaultRoute, action)
{
  switch (action.type) {
    case SET_ROUTE:
      const {path, param} = action;
      return {path, param};
      break;
    default:
      return route;
  }
};

export function setRoute(path, param)
{
  return {type: SET_ROUTE, path, param};
};

export default RouteReducer;
