import {combineReducers, createStore, compose } from 'redux';
import user                                     from './User';
import figures                                  from './Figure';
import route                                    from './Route';

const reducer = combineReducers({
  user,
  figures,
  route,
  maxUpload : (s = null) => s
});

let composed = f => f;

if (__DEV__)
{
  if (window.devToolsExtension)
  {
    composed = window.devToolsExtension();
  }
}

const finalCreateStore = compose(composed)(createStore);

const store = finalCreateStore(reducer, Object.assign({}, window.sobrassada));
delete window.sobrassada;

if (__DEV__)
{
  window.sobrassada =
  {
    getState : store.getState
  };
}

export default store;
