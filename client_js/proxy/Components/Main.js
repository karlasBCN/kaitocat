import React  from 'react';
import moment from 'moment';
import Panel  from 'react-bootstrap/lib/Panel';

class Main extends React.Component
{
  render()
  {
    var props          = this.props;
    var lastConnection = moment.unix(props.lastConnection).format('MMMM Do YYYY, HH:mm:mm');

    return <Panel header='PROXY' bsStyle='primary'>
      Last Raspberry Connection : {lastConnection}
    </Panel>;
  }
};

export default Main;
