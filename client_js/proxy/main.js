import React    from 'react';
import ReactDOM from 'react-dom';
import Main     from './Components/Main';
import getter   from './getter';

require('../../less/proxy/main.less');

var mainComponent = null;
var render  = function(props)
{
  mainComponent = ReactDOM.render(React.createElement(Main, props), document.getElementById('app'));
};

getter.on('get', function(msg){
  render(msg);
});

getter.startUpdate();

render(window.first);
delete window.first;
