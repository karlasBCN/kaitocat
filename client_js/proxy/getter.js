import xhr    from 'xhr';
import Events from 'ampersand-events';

var ajax = function(callback)
{
  var params =
  {
    uri     : '/getter',
    method  :  'POST',
    body    : '',
    headers : {'Content-Type': 'application/json'}
  };

  xhr(params, function (err, resp, body)
  {
    if (err)
    {
      throw 'Error with ajax request';
    }
    callback(JSON.parse(body));
  });
};

var getter =
{
  startUpdate : function()
  {
    var _this = this;
    setInterval(function()
    {
      ajax(res => _this.trigger('get', res));
    }, 5000);
  }
};

Events.createEmitter(getter);

export default getter;
