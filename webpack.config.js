var webpack           = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var isDev             = (process.env.NODE_ENV === 'dev');
var entry             =
{
  sobrassada : './client_js/sobrassada/main.js',
  proxy      : './client_js/proxy/main.js'
};

if (isDev)
{
  entry['only-dev-server'] = 'webpack/hot/only-dev-server';
  entry.client             = 'webpack-dev-server/client?http://localhost:8000';
};

console.log("IS DEV : " + (isDev ? "YES" : "NO"));

var config  =
{
  entry   : entry,
  devtool : isDev ? 'source-map' : false,
  devServer:
  {
    headers: { "Access-Control-Allow-Origin": "*" }
  },
  output  :
  {
    chunkFilename : '[name].js',
    filename      : '[name].js',
    path          : './public/',
    publicPath    : (isDev ? 'http://localhost:8080' : '') + '/public/'
  },
  module:
  {
    loaders:
    [
      {
        test    : /\.js$/,
        exclude : /(node_modules)/,
        loader  : isDev ? 'react-hot!babel!eslint-loader' : 'babel'
      },
      {
        test   : /\.less$/,
        loader : ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader')
      },
      //Fonts
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }
    ]
  },
  plugins:
  [
    new webpack.DefinePlugin(
    {
      'process.env' : {'NODE_ENV': JSON.stringify(process.env.NODE_ENV)},
      __DEV__       : isDev,
    }),
    new ExtractTextPlugin('[name].css')
  ]
};

if (isDev)
{
 config.eslint = {configFile: '.eslintrc'};
}

module.exports = config;
