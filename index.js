var async  = require('async'),
    chalk  = require('chalk');

var db_task = function(end_task){
  //Connect to DB
  var db = require('./src/db');
  db.init(function(){
    console.log(chalk.green('db ok'));
    end_task(null);
  });
};

var server_task = function(end_task){
  //Hapi server
  var server = require('./src/server');
  server.init(function(){
    console.log(chalk.green('server ok'));
    end_task(null);
  });
};

async.series([db_task, server_task], function(err, results){
  console.log(chalk.blue('Kaito.cat ready!!!'));
});
